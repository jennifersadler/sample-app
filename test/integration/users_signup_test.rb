require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test "Invalid sign up information" do
  	get signup_path
  	assert_no_difference 'User.count' do
  		post users_path, params: {user:{name: "", email: "invalid@example", password:"no", confirm_password: "yes"}}

  	end
  	assert_template 'users/new'
    assert_select 'div#error_explanation'
    assert_select 'div.field_with_errors'

  end

  test "valid signup information" do
    get signup_path
    assert_difference 'User.count' do
      post users_path, params: {user: {name: "joe Schmoe", email:"joe@example.com", password:"Password1", confirm_password:"Password1"}}

    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.count == 0

  end
end
